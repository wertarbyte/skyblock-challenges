# Skyblock Challenges
This is our repository to manage skyblock challenges for [SkyblockPlus][SBP]. We use most of them on [our server][craften-server].

## Contribution guidelines
We love pull requests, so feel free to send us your challenge ideas! Just make sure to follow these simple rules that make our lives somewhat easier.

* Only submit valid YAML with tested challenges
* Challenge names and descriptions should be written in english, without mistakes
* Challenge names should be written in [Title Case][title-case]
* Challenge descriptions should be full sentences

## License
We use the very permissive WTFPL. So do what you want to do, credits would be awesome but aren't required.

[SBP]:https://bitbucket.org/wertarbyte/skyblockplus
[craften-server]:http://craften.de/server
[title-case]:http://www.grammar-monster.com/lessons/capital_letters_title_case.htm


